$( document ).ready(function() {

  // init carousel
  var swiper = new Swiper('.swiper-container', {
    pagination: {
      el: '.swiper-pagination',
    },
  })


  // close all filters on init
  $('.panel-options').slideUp()


  // add more items
  var products = [
  	{ title: 'Botín', price: 800.44, category: 'Botines', image: 'http://placehold.it/225x225', brand: 'nike' },
  	{ title: 'Ojotas', price: 300.99, category: 'Ojotas', image: 'http://placehold.it/225x225', brand: 'adidas' },
  	{ title: 'Zapatillas', price: 1120.00, category: 'Calzado', image: 'http://placehold.it/225x225', brand: 'puma' },
  	{ title: 'Short', price: 320.44, category: 'Vestimenta', image: 'http://placehold.it/225x225', brand: 'nike' },
  	{ title: 'Pantalón', price: 360.44, category: 'Natación', image: 'http://placehold.it/225x225', brand: 'nike' }
  ];

  for(var i = 0; i < products.length; i++){
    $('section.gallery').append(`

      <article class="item-product">
        <div>
          <img src="${ products[i].image }"
               alt="${ products[i].title }">
        </div>
        <span class="offer">Oferta</span>
        <h4>${ products[i].title }</h4>

        <div class="store-data">
          <span>$${ products[i].price }</span>
          <i class="icon-store icon-${ products[i].brand }">
          </i>
        </div>
        <p>${ products[i].category }</p>
      </article>
    `)

  }


  // toggle filter
  $('.options').click(function() {

    var opt = $(this)

    if(opt.hasClass('gender'))
      toggleFilter('.gender')

    if(opt.hasClass('sport'))
      toggleFilter('.sport')

    if(opt.hasClass('colors'))
      toggleFilter('.colors')
  })

  function toggleFilter(filter) {

    // add class active
    $('.options' + filter).toggleClass('active')
    $('.panel-options' + filter).toggleClass('active')
    $('.panel-options' + filter).slideToggle('fast');
  }



  // hover product
  $('article.item-product').hover(

    function() {
      $(this).find('h4').css('font-size', '22px')
      $(this).find('.store-data span').css('color', '#a5e55b')
    },
    function() {
      $(this).find('h4').css('font-size', '16px')
      $(this).find('.store-data span').css('color', '#D44700')
    }
  )


});
