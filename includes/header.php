<header>
  <div class="header-top">

    <div class="container row-1">
      <a href="#">
        <i class="icon icon-pin"></i>
        Sucursales
      </a>
      <a href="#">
        <i class="icon icon-label"></i>
        Como comprar
      </a>
      <a href="#">
        Mis pedidos
      </a>
    </div>

    <div class="container row-2">
      <div>
        <img src="images/logo.jpg" alt="image logo">
      </div>

       <div class="var-bus">
         <input type="search" name="search" placeholder="Buscar por productos, deportes o marcas">
         <div class="btn-bus"></div>
       </div>

      <div>
        <img src="images/HeaderHomeAbril.gif" alt="">
      </div>
    </div>
  </div>

  <div class="header-bottom">
    <div class="container">
      <nav>
        <a href="#">Hombre</a>
        <a href="#">Mujer</a>
        <a href="#">Niño</a>
      </nav>
      <div>
        <p><span>Carrito</span> <i class="icon icon-cart"></i> 0  $0,00</p>
      </div>
    </div>
  </div>

</header>
